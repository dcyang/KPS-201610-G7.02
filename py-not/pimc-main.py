#!/usr/bin/env python3
# coding: utf-8

#get_ipython().magic('matplotlib inline')
#get_ipython().magic("config InlineBackend.figure_format = 'svg'")
from time import perf_counter
from math import pi, sqrt, exp, log, log1p, erfc, isinf
from random import seed, random, randrange, gauss

import numpy as np
from scipy import stats
import matplotlib
import matplotlib.pyplot as plt


# ## System variables

T = 0.5
N_sp = 2
N = [13,7]

mass = 1.0
omega = 1.0

g = 4.0


# ## Simulation parameters
# Assume reduced units, i.e., $k_B = \hbar = m = \omega = 1$.

N_ts = 64
nWarmup = 1000
#nPasses = 2000
tWall = 300.0

lEnergy = 1000


# Some derived / internal variables

beta = 1.0 / T
tau = beta / N_ts
lam = 0.5/mass   # hbar^2 / 2m
eff_lam = lam*2.0
displaceSigma2 = 2.0 * lam * tau
displaceSigma = sqrt(displaceSigma2)
sqrtPi = sqrt(pi)

omega2 = omega*omega


# ## Initialize configuration

#seed(44919)
#cnf = np.zeros((N, N_ts))
#cnf = np.random.randn(N, N_ts)
#buf = np.array([-0.63835765, -0.64883886, -1.01591382, -0.31398414, 0.30037767, 0.86562668, 
#      0.41348673, 0.57145019, 0.55375378, 0.45349816, 0.43466229, 0.47286036,
#      1.04875713, 0.85895752, 1.69012416, 0.33557273, 0.58638789, -0.0754684,
#      0.00988711, -0.68279235, -0.35234596, 0.02642818, -0.03683141, -0.50405597,
#      -0.45592092, -0.28019833, -0.29855183, 0.31688496, 1.18928, 0.05344008,
#      0.10807762, -0.26408834])
#cnf = list()
#for j in range(N_sp):
#    cnf.append(buf.copy())
#    for i in range(1,N[j]):
#        cnf[-1] = np.vstack((cnf[-1],buf+i*displaceSigma))
#    cnf[-1] -= np.mean(cnf[-1])
cnf = list()
buf = np.random.randn(N[0], N_ts)
cnf.append(np.sort(buf, axis=0) * 2.0)
buf = np.random.randn(N[1], N_ts)
cnf.append(np.sort(buf, axis=0) * 2.0)


# ## Functions

aCoeff_K = tau / (displaceSigma2 + displaceSigma2)
aCoeff_U1 = 0.5 * tau

def getTotalAction():
    K = 0.0
    U1 = 0.0
    U2a = U2b = 0.0
    
    # fermi nodal
    for j in range(N_sp):
        for i in range(N_ts):
            i1 = i + 1
            if i1 >= N_ts: i1 -= N_ts
            for k in range(1,N[j]):
                dx = cnf[j][k,i] - cnf[j][k-1,i]
                dy = cnf[j][k,i1] - cnf[j][k-1,i1]
                if dx <= 0.0 or dy <= 0.0: return float('inf')
                U2a -= log1p(-exp(-dx*dy/displaceSigma2))

    # ||
    for j in range(N_sp):
        for i in range(N_ts):
            i1 = i + 1
            if i1 >= N_ts: i1 -= N_ts
            for k in range(N[j]):
                dx = cnf[j][k,i1] - cnf[j][k,i]
                K += 0.5 * log(pi*(displaceSigma2+displaceSigma2)) + (dx*dx/(displaceSigma2+displaceSigma2))
                #K += dx*dx
        #K *= aCoeff_K

    for j in range(N_sp):
        for i in range(N_ts):
            i1 = i + 1
            if i1 >= N_ts: i1 -= N_ts
            for k in range(N[j]):
                U1 += 0.25 * tau * (cnf[j][k,i1]*cnf[j][k,i1] + cnf[j][k,i]*cnf[j][k,i])
                #U1 += cnf[k,i] * cnf[k,i]
        #U1 *= aCoeff_U1
        # LBiB eq.3.6

    # ==
    s = 0.5*g*sqrt(tau/eff_lam)
    z = 1.0/sqrt(4.0*eff_lam*tau)
    for j0 in range(N_sp):
        for j1 in range(j0+1,N_sp):
            for i in range(N_ts):
                i1 = i + 1
                if i1 >= N_ts: i1 -= N_ts
                for k0 in range(N[j0]):
                    for k1 in range(N[j1]):
                        dx = cnf[j1][k1,i] - cnf[j0][k0,i]
                        dy = cnf[j1][k1,i1] - cnf[j0][k0,i1]
                        r = z*(abs(dx) + abs(dy))
                        dR = dx - dy
                        U2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))

    #print(K, U1, U2a, U2b, "\t", K+U1+U2a+U2b)
    return K+U1+U2a+U2b


def getSliceActionDiff(sp, pt, ts0, xNew):
    dK = 0.0
    dU1 = 0.0
    dU2a = dU2b = 0.0
    
    ts9 = ts0 - 1
    if ts9 < 0: ts9 += N_ts
    ts1 = ts0 + 1
    if ts1 >= N_ts: ts1 -= N_ts
    # not sure if modulo % operator is as efficient
    
    # fermi nodal
    foundL = pt > 0
    foundR = pt < N[sp]-1
    if foundL and foundR:
        dxLNew = xNew - cnf[sp][pt-1,ts0]
        dxRNew = cnf[sp][pt+1,ts0] - xNew
        if dxLNew <= 0.0 or dxRNew <= 0.0: return float('inf')
        dx9L = cnf[sp][pt,ts9] - cnf[sp][pt-1,ts9]
        dx1L = cnf[sp][pt,ts1] - cnf[sp][pt-1,ts1]
        dxL = cnf[sp][pt,ts0] - cnf[sp][pt-1,ts0]
        dx9R = cnf[sp][pt+1,ts9] - cnf[sp][pt,ts9]
        dx1R = cnf[sp][pt+1,ts1] - cnf[sp][pt,ts1]
        dxR = cnf[sp][pt+1,ts0] - cnf[sp][pt,ts0]
        
        dU2a += (-log1p(-exp(-dx9L*dxLNew/displaceSigma2))
                 -log1p(-exp(-dx9R*dxRNew/displaceSigma2))
                 -log1p(-exp(-dxLNew*dx1L/displaceSigma2))
                 -log1p(-exp(-dxRNew*dx1R/displaceSigma2)))
        dU2a -= (-log1p(-exp(-dx9L*dxL/displaceSigma2))
                 -log1p(-exp(-dx9R*dxR/displaceSigma2))
                 -log1p(-exp(-dxL*dx1L/displaceSigma2))
                 -log1p(-exp(-dxR*dx1R/displaceSigma2)))
    elif foundL:
        dxLNew = xNew - cnf[sp][pt-1,ts0]
        if dxLNew <= 0.0: return float('inf')
        dx9L = cnf[sp][pt,ts9] - cnf[sp][pt-1,ts9]
        dx1L = cnf[sp][pt,ts1] - cnf[sp][pt-1,ts1]
        dxL = cnf[sp][pt,ts0] - cnf[sp][pt-1,ts0]

        dU2a += (-log1p(-exp(-dx9L*dxLNew/displaceSigma2))
                 -log1p(-exp(-dxLNew*dx1L/displaceSigma2)))
        dU2a -= (-log1p(-exp(-dx9L*dxL/displaceSigma2))
                 -log1p(-exp(-dxL*dx1L/displaceSigma2)))
    elif foundR:
        dxRNew = cnf[sp][pt+1,ts0] - xNew
        if dxRNew <= 0.0: return float('inf')
        dx9R = cnf[sp][pt+1,ts9] - cnf[sp][pt,ts9]
        dx1R = cnf[sp][pt+1,ts1] - cnf[sp][pt,ts1]
        dxR = cnf[sp][pt+1,ts0] - cnf[sp][pt,ts0]
        
        dU2a += (-log1p(-exp(-dx9R*dxRNew/displaceSigma2))
                 -log1p(-exp(-dxRNew*dx1R/displaceSigma2)))
        dU2a -= (-log1p(-exp(-dx9R*dxR/displaceSigma2))
                 -log1p(-exp(-dxR*dx1R/displaceSigma2)))
    #else nothing

    dxNew = xNew - cnf[sp][pt,ts9]
    dxOld = cnf[sp][pt,ts0] - cnf[sp][pt,ts9]
    dK += (dxNew*dxNew - dxOld*dxOld)/(displaceSigma2+displaceSigma2)
    dxNew = cnf[sp][pt,ts1] - xNew
    dxOld = cnf[sp][pt,ts1] - cnf[sp][pt,ts0]
    dK += (dxNew*dxNew - dxOld*dxOld)/(displaceSigma2+displaceSigma2)
    #dK *= aCoeff_K
    
    dU1 += 0.5*tau*(xNew*xNew - cnf[sp][pt,ts0]*cnf[sp][pt,ts0])
    #dU1 += xNew*xNew - cnf[pt,ts0]*cnf[pt,ts0]
    #dU1 *= aCoeff_U1
    
    s = 0.5*g*sqrt(tau/eff_lam)
    z = 1.0/sqrt(4.0*eff_lam*tau)
    for sp1 in list(range(sp))+list(range(sp+1,N_sp)):
        for pt1 in range(N[sp1]):
            dx9 = cnf[sp][pt,ts9] - cnf[sp1][pt1,ts9]
            dx1 = cnf[sp][pt,ts1] - cnf[sp1][pt1,ts1]
            dx0 = cnf[sp][pt,ts0] - cnf[sp1][pt1,ts0]
            dxNew = xNew - cnf[sp1][pt1,ts0]
            
            # addition of a negative term
            r = z*(abs(dx9) + abs(dxNew))
            dR = dx9 - dxNew
            dU2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
            r = z*(abs(dxNew) + abs(dx1))
            dR = dxNew - dx1
            dU2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))

            # subtraction of a negative term
            r = z*(abs(dx9) + abs(dx0))
            dR = dx9 - dx0
            dU2b += log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
            r = z*(abs(dx0) + abs(dx1))
            dR = dx0 - dx1
            dU2b += log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))

    return dK+dU1+dU2a+dU2b


eCoeff_K = T / (displaceSigma2 + displaceSigma2)
eCoeff_U1 = 1.0 / (N_ts + N_ts)

#print(0.5*g*sqrt(tau/eff_lam), 1.0/sqrt(4.0*eff_lam*tau))
def getLocalEnergy():
    K = 0.0
    U1 = 0.0
    U2a = U2b = 0.0
    tc_buffer = 0.0
    
    # assert: we're in an allowed state, ie. energy assumed finite
    for j in range(N_sp):
        for i in range(N_ts):
            i1 = i + 1
            if i1 >= N_ts: i1 -= N_ts
            for k in range(1,N[j]):
                dx = cnf[j][k,i] - cnf[j][k-1,i]
                dy = cnf[j][k,i1] - cnf[j][k-1,i1]
                tc = dx*dy / (displaceSigma2 * tau * (exp(dx*dy/displaceSigma2) - 1.0)) / N_ts
                U2a += dx*dy / (displaceSigma2 * tau * (exp(dx*dy/displaceSigma2) - 1.0)) / N_ts - tc
                tc_buffer += tc
    
    for j in range(N_sp):
        for i in range(N_ts):
            i1 = i + 1
            if i1 >= N_ts: i1 -= N_ts
            for k in range(N[j]):
                #U1 += cnf[j][k,i] * cnf[j][k,i]
                tc = -0.25 * (cnf[j][k,i1]*cnf[j][k,i1] + cnf[j][k,i]*cnf[j][k,i]) / N_ts
                U1 += 0.25 * (cnf[j][k,i1]*cnf[j][k,i1] + cnf[j][k,i]*cnf[j][k,i]) / N_ts - tc
                tc_buffer += tc
        #U1 *= eCoeff_U1

    s = 0.5*g*sqrt(tau/eff_lam)
    z = 1.0/sqrt(4.0*eff_lam*tau)
    for j0 in range(N_sp):
        for j1 in range(j0+1,N_sp):
            for i in range(N_ts):
                i1 = i + 1
                if i1 >= N_ts: i1 -= N_ts
                for k0 in range(N[j0]):
                    for k1 in range(N[j1]):
                        dx = cnf[j1][k1,i] - cnf[j0][k0,i]
                        dy = cnf[j1][k1,i1] - cnf[j0][k0,i1]
                        r = z*(abs(dx) + abs(dy))
                        dR = dx - dy
                        buf1 = exp(s*(s-r-r)+z*z*dR*dR)
                        buf2 = erfc(r-s)
                        tc = eff_lam*sqrtPi*s*buf1*((1.0+2.0*(s*s-2*s*r+z*z*dR*dR))*buf2+(2.0/sqrtPi)*exp(-(r-s)*(r-s))*(s-r))/(2.0*tau*eff_lam*(1.0+sqrtPi*s*buf1*buf2)*N_ts)
                        U2b += (-sqrtPi*s*buf1*((1.0+2.0*(s*s-z*z*dR*dR))*buf2 + (2.0/sqrtPi)*exp(-(r-s)*(r-s))*(r+s))/(2.0*tau*(1.0+sqrtPi*s*buf1*buf2)*N_ts) - tc)
                        tc_buffer += tc
    
    for j in range(N_sp):
        for i in range(N_ts):
            i1 = i + 1
            if i1 >= N_ts: i1 -= N_ts
            for k in range(N[j]):
                dx = cnf[j][k,i1] - cnf[j][k,i]
                #dx = cnf[k,(i+1)%N_ts] - cnf[k,i]
                #K -= dx*dx
                #print(0.5 * T * (1.0 - N_ts * T * dx * dx), N_ts, T, dx)
                K += 0.5 * T * (1.0 - N_ts * T * dx * dx)
        #K *= eCoeff_K
        #K += 0.5 * T * N[j] * N_ts
    K += tc_buffer

    #print(K, U1, U2a, U2b, K+U1+U2a+U2b)
    return K+U1+U2a+U2b


# ## Main loop

S = getTotalAction()
assert not isinf(S)

eLocals = np.zeros(lEnergy)

# Warmup
for z in range(nWarmup):
    for j in range(N_sp):
        for i in range(N_ts):
            ts = randrange(N_ts)
            for k in range(N[j]):
                pt = randrange(N[j])
                #xNew = cnf[pt,ts] + uniform(-displaceSigma, displaceSigma)
                xNew = gauss(cnf[j][pt,ts], displaceSigma)
                dS = getSliceActionDiff(j, pt, ts, xNew)

                if (random() < exp(-dS)):
                    # update coords
                    cnf[j][pt,ts] = xNew
                    # update S ... hold on ... is it even necessary?
                    S += dS
print(S)

S = getTotalAction()
assert not isinf(S)

z = 0
nAccepts = nAttempts = 0
tStart = perf_counter()
tEnd = tStart + tWall

while perf_counter() < tEnd:
    for j in range(N_sp):
        for i in range(N_ts):
            ts = randrange(N_ts)
            for k in range(N[j]):
                pt = randrange(N[j])
                #xNew = cnf[pt,ts] + uniform(-displaceSigma, displaceSigma)
                xNew = gauss(cnf[j][pt,ts], displaceSigma)
                dS = getSliceActionDiff(j, pt, ts, xNew)

                if (random() < exp(-dS)):
                    # update coords
                    cnf[j][pt,ts] = xNew
                    # update S ... hold on ... is it even necessary?
                    S += dS
                    nAccepts += 1
                nAttempts += 1
                
    eLocals[z % lEnergy] = getLocalEnergy()
    z += 1
                
print(S)


# ## Output statistics

# do stats
print(np.mean(eLocals), u'±', stats.sem(eLocals))
print(nAccepts / nAttempts, 'of', nAttempts)

#fig = plt.figure()
#ax = fig.add_subplot(111)
#
#for k in range(N[0]):
#    ax.plot(cnf[0][k,:],range(N_ts), 'ro-', alpha=0.5)
#for k in range(N[1]):
#    ax.plot(cnf[1][k,:],range(N_ts), 'bo-', alpha=0.5)
#
#x0 = 1.25*max(np.max(np.abs(cnf[0])),np.max(np.abs(cnf[1])))
#dx = 0.05
#y, x = np.mgrid[slice(-1,N_ts+1), slice(-x0,x0,dx)]
#f = np.exp(-x*x/(2*displaceSigma2*max(N)*max(N)))
#plt.contourf(x, y, f, cmap=matplotlib.cm.Greys_r, alpha=0.2)
#
#ax.set_xlabel('x')
#ax.set_xlim(-x0, x0)
#ax.set_ylabel('Imaginary time steps')
#ax.set_ylim(-1,N_ts)
#plt.show()
#
##plt.savefig('pathdist.svg', format='svg')
##plt.savefig('pathdist.png', format='png')
