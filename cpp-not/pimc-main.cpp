#include <iostream>
#include <limits>
#include <cmath>
#include <vector>
#include <algorithm>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics_double.h>

using namespace std;


// global variables ...
// (Yes, I know this is bad practice.)
// (Fix according to Effective "___" series by Scott Meyers if this bothers you.)
double temperature = 0.5;
int n_sp = 2;
int n_pt[] = {13, 7};

// assume reduced units
double mass = 1.0, omega = 1.0;
double g = 4.0;

int n_ts = 64;
int nWarmup = 1000;
time_t runSeconds = 120;
int lEnergy = 5000;

double beta = 1.0 / temperature;
double tau = beta / double(n_ts);
double lam = 0.5 / mass, lam_reduced = 2.0 * lam;
double displaceSigma2 = 2.0 * lam * tau;
double displaceSigma = sqrt(displaceSigma2);
double sqrtPi = sqrt(M_PI);
double omega2 = omega * omega;

const double s = 0.5 * g * sqrt(tau/lam_reduced);
const double z = 1.0 / sqrt(4.0 * lam_reduced * tau);

const gsl_rng_type *rng_type;
gsl_rng *rng;
double ***cnf;


double getTotalAction() {
  double K = 0.0, U1 = 0.0, U2a = 0.0,  U2b = 0.0;
  /*     kinetic, external, like pairs, unlike pairs */

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      int i_ = (i + 1) % n_ts;  // modulo could be slower than conditionals
      for (int k = 1; k < n_pt[j]; ++k) {
        double dx = cnf[j][k][i] - cnf[j][k-1][i];
        double dy = cnf[j][k][i_] - cnf[j][k-1][i_];
        if (dx <= 0.0 || dy <= 0.0) return numeric_limits<double>::infinity();
        U2a += (-log1p(-exp(-dx*dy/displaceSigma2)));
      }
    }
  }

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      int i_ = (i + 1) % n_ts;
      for (int k = 0; k < n_pt[j]; ++k) {
        double dx = cnf[j][k][i_] - cnf[j][k][i];
        K += 0.5 * log(M_PI*(displaceSigma2+displaceSigma2)) + (dx*dx/(displaceSigma2+displaceSigma2));
      }
    }
  }

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      int i_ = (i + 1) % n_ts;
      for (int k = 0; k < n_pt[j]; ++k) {
        U1 += 0.25 * tau * (cnf[j][k][i_]*cnf[j][k][i_] + cnf[j][k][i]*cnf[j][k][i]);
      }
    }
  }

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      int i_ = (i + 1) % n_ts;
      for (int k = 0; k < n_pt[j]; ++k) {
        for (int j_ = j+1; j_ < n_sp; ++j_) {
          for (int k_ = 0; k_ < n_pt[j_]; ++k_) {
            double dx = cnf[j_][k_][i] - cnf[j][k][i];
            double dy = cnf[j_][k_][i_] - cnf[j][k][i_];
            double r = z*(fabs(dx) + fabs(dy));
            double dR = dx - dy;
            U2b += (-log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s)));
          }
        }
      }
    }
  }

  //cout << "getTotalAction():\t" << K << " " << U1 << " " << U2a << " " << U2b << endl;
  return K + U1 + U2a + U2b;
}


double getSliceActionDiff(int sp, int pt, int ts0, double xNew) {
  double dK = 0.0, dU1 = 0.0, dU2a = 0.0, dU2b = 0.0;
  /*     kinetic,  external,  like pairs, unlike pairs */
  int ts9 = (ts0 - 1 + n_ts) % n_ts;
  int ts1 = (ts0 + 1) % n_ts;

  bool foundL = pt > 0, foundR = pt < n_pt[sp] - 1;
  if (foundL && foundR) {
    double dxLNew = xNew - cnf[sp][pt-1][ts0],
           dxRNew = cnf[sp][pt+1][ts0] - xNew;
    if (dxLNew <= 0.0 || dxRNew <= 0.0) return numeric_limits<double>::infinity();
    double dx9L = cnf[sp][pt][ts9] - cnf[sp][pt-1][ts9],
           dx1L = cnf[sp][pt][ts1] - cnf[sp][pt-1][ts1],
           dxL = cnf[sp][pt][ts0] - cnf[sp][pt-1][ts0];
    double dx9R = cnf[sp][pt+1][ts9] - cnf[sp][pt][ts9],
           dx1R = cnf[sp][pt+1][ts1] - cnf[sp][pt][ts1],
           dxR = cnf[sp][pt+1][ts0] - cnf[sp][pt][ts0];
    dU2a += (-log1p(-exp(-dx9L*dxLNew/displaceSigma2)) - log1p(-exp(-dx9R*dxRNew/displaceSigma2))
             -log1p(-exp(-dxLNew*dx1L/displaceSigma2)) - log1p(-exp(-dxRNew*dx1R/displaceSigma2)));
    dU2a -= (-log1p(-exp(-dx9L*dxL/displaceSigma2)) - log1p(-exp(-dx9R*dxR/displaceSigma2))
             -log1p(-exp(-dxL*dx1L/displaceSigma2)) - log1p(-exp(-dxR*dx1R/displaceSigma2)));
  }
  else if (foundL) {
    double dxLNew = xNew - cnf[sp][pt-1][ts0];
    if (dxLNew <= 0.0) return numeric_limits<double>::infinity();
    double dx9L = cnf[sp][pt][ts9] - cnf[sp][pt-1][ts9],
           dx1L = cnf[sp][pt][ts1] - cnf[sp][pt-1][ts1],
           dxL = cnf[sp][pt][ts0] - cnf[sp][pt-1][ts0];
    dU2a += (-log1p(-exp(-dx9L*dxLNew/displaceSigma2)) - log1p(-exp(-dxLNew*dx1L/displaceSigma2)));
    dU2a -= (-log1p(-exp(-dx9L*dxL/displaceSigma2)) - log1p(-exp(-dxL*dx1L/displaceSigma2)));
  }
  else if (foundR) {
    double dxRNew = cnf[sp][pt+1][ts0] - xNew;
    if (dxRNew <= 0.0) return numeric_limits<double>::infinity();
    double dx9R = cnf[sp][pt+1][ts9] - cnf[sp][pt][ts9],
           dx1R = cnf[sp][pt+1][ts1] - cnf[sp][pt][ts1],
           dxR = cnf[sp][pt+1][ts0] - cnf[sp][pt][ts0];
    dU2a += (-log1p(-exp(-dx9R*dxRNew/displaceSigma2)) - log1p(-exp(-dxRNew*dx1R/displaceSigma2)));
    dU2a -= (-log1p(-exp(-dx9R*dxR/displaceSigma2)) - log1p(-exp(-dxR*dx1R/displaceSigma2)));
  }
  //else nothing;

  double dxNew, dxOld;
  dxNew = xNew - cnf[sp][pt][ts9];
  dxOld = cnf[sp][pt][ts0] - cnf[sp][pt][ts9];
  dK += (dxNew*dxNew - dxOld*dxOld)/(displaceSigma2+displaceSigma2);
  dxNew = cnf[sp][pt][ts1] - xNew;
  dxOld = cnf[sp][pt][ts1] - cnf[sp][pt][ts0];
  dK += (dxNew*dxNew - dxOld*dxOld)/(displaceSigma2+displaceSigma2);

  dU1 += 0.5*tau*(xNew*xNew - cnf[sp][pt][ts0]*cnf[sp][pt][ts0]);

  for (int sp1 = 0; sp1 < sp; ++sp1) {
    for (int pt1 = 0; pt1 < n_pt[sp1]; ++pt1) {
      double dx9 = cnf[sp][pt][ts9] - cnf[sp1][pt1][ts9],
             dx1 = cnf[sp][pt][ts1] - cnf[sp1][pt1][ts1],
             dx0 = cnf[sp][pt][ts0] - cnf[sp1][pt1][ts0],
             dxNew = xNew - cnf[sp1][pt1][ts0];

      double r, dR;
      // addition of a negative term
      r = z*(fabs(dx9) + fabs(dxNew));
      dR = dx9 - dxNew;
      dU2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s));
      r = z*(fabs(dxNew) + fabs(dx1));
      dR = dxNew - dx1;
      dU2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s));

      // subtraction of a negative term
      r = z*(fabs(dx9) + fabs(dx0));
      dR = dx9 - dx0;
      dU2b += log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s));
      r = z*(fabs(dx0) + fabs(dx1));
      dR = dx0 - dx1;
      dU2b += log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s));
    }
  }
  for (int sp1 = sp+1; sp1 < n_sp; ++sp1) {
    for (int pt1 = 0; pt1 < n_pt[sp1]; ++pt1) {
      double dx9 = cnf[sp][pt][ts9] - cnf[sp1][pt1][ts9],
             dx1 = cnf[sp][pt][ts1] - cnf[sp1][pt1][ts1],
             dx0 = cnf[sp][pt][ts0] - cnf[sp1][pt1][ts0],
             dxNew = xNew - cnf[sp1][pt1][ts0];

      double r, dR;
      // addition of a negative term
      r = z*(fabs(dx9) + fabs(dxNew));
      dR = dx9 - dxNew;
      dU2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s));
      r = z*(fabs(dxNew) + fabs(dx1));
      dR = dxNew - dx1;
      dU2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s));

      // subtraction of a negative term
      r = z*(fabs(dx9) + fabs(dx0));
      dR = dx9 - dx0;
      dU2b += log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s));
      r = z*(fabs(dx0) + fabs(dx1));
      dR = dx0 - dx1;
      dU2b += log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s));
    }
  }

  //cout << "getSliceActionDiff():\t" << dK << " " << dU1 << " " << dU2a << " " << dU2b << endl;
  return dK + dU1 + dU2a + dU2b;
}


double getLocalEnergy() {
  double K = 0.0, U1 = 0.0, U2a = 0.0,  U2b = 0.0;
  /*     kinetic, external, like pairs, unlike pairs */
  double tc_buffer = 0.0;

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      int i_ = (i + 1) % n_ts;  // modulo could be slower than conditionals
      for (int k = 1; k < n_pt[j]; ++k) {
        double dx = cnf[j][k][i] - cnf[j][k-1][i];
        double dy = cnf[j][k][i_] - cnf[j][k-1][i_];
        double tc = dx*dy / (displaceSigma2 * tau * (exp(dx*dy/displaceSigma2) - 1.0) * double(n_ts));
        U2a += dx*dy / (displaceSigma2 * tau * (exp(dx*dy/displaceSigma2) - 1.0) * double(n_ts)) - tc;
        tc_buffer += tc;
      }
    }
  }

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      int i_ = (i + 1) % n_ts;
      for (int k = 0; k < n_pt[j]; ++k) {
        double tc = -0.25 * (cnf[j][k][i_]*cnf[j][k][i_] + cnf[j][k][i]*cnf[j][k][i]) / double(n_ts);
        U1 += 0.25 * (cnf[j][k][i_]*cnf[j][k][i_] + cnf[j][k][i]*cnf[j][k][i]) / double(n_ts) - tc;
        tc_buffer += tc;
      }
    }
  }

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      int i_ = (i + 1) % n_ts;
      for (int k = 0; k < n_pt[j]; ++k) {
        for (int j_ = j+1; j_ < n_sp; ++j_) {
          for (int k_ = 0; k_ < n_pt[j_]; ++k_) {
            double dx = cnf[j_][k_][i] - cnf[j][k][i];
            double dy = cnf[j_][k_][i_] - cnf[j][k][i_];
            double r = z*(fabs(dx) + fabs(dy)),
                   dR = dx - dy;
            double buf1 = exp(s*(s-r-r)+z*z*dR*dR),
                   buf2 = erfc(r-s);
            double tc = lam_reduced*sqrtPi*s*buf1*((1.0+2.0*(s*s-2*s*r+z*z*dR*dR))*buf2+M_2_SQRTPI*exp(-(r-s)*(r-s))*(s-r))/(2.0*tau*lam_reduced*(1.0+sqrtPi*s*buf1*buf2)*double(n_ts));
            U2b += (-sqrtPi*s*buf1*((1.0+2.0*(s*s-z*z*dR*dR))*buf2 + M_2_SQRTPI*exp(-(r-s)*(r-s))*(r+s))/(2.0*tau*(1.0+sqrtPi*s*buf1*buf2)*double(n_ts)) - tc);
            tc_buffer += tc;
          }
        }
      }
    }
  }

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      int i_ = (i + 1) % n_ts;
      for (int k = 0; k < n_pt[j]; ++k) {
        double dx = cnf[j][k][i_] - cnf[j][k][i];
        K += 0.5 * temperature * (1.0 - n_ts * temperature * dx * dx);
      }
    }
  }
  K += tc_buffer;

  //cout << "getLocalEnergy():\t" << K << " " << U1 << " " << U2a << " " << U2b << " " << (K+U1+U2a+U2b) << endl;
  return K + U1 + U2a + U2b;
}


int main() {
  gsl_rng_env_setup();
  rng_type = gsl_rng_default;
  rng = gsl_rng_alloc(rng_type);
  unsigned long rngSeed = (unsigned long)time(NULL);
  gsl_rng_set(rng, rngSeed);

  cnf = new double ** [n_sp];
  for (int j = 0; j < n_sp; ++j) {
    cnf[j] = new double * [n_pt[j]];
    for (int k = 0; k < n_pt[j]; ++k) cnf[j][k] = new double [n_ts];
  }

  for (int j = 0; j < n_sp; ++j) {
    for (int i = 0; i < n_ts; ++i) {
      vector<double> buf;
      for (int k = 0; k < n_pt[j]; ++k) buf.push_back(gsl_ran_gaussian_ziggurat(rng, 2.0));
      sort(buf.begin(), buf.end());
      for (int k = 0; k < n_pt[j]; ++k) cnf[j][k][i] = buf[k];
      buf.clear();
    }
  }

  double S = getTotalAction();  // total action
  cout << "S = " << S << endl;
  unsigned nAttempts = 0, nAccepts = 0;
  int n;
  for (n = 0; n < nWarmup; ++n) {
    for (int j = 0; j < n_sp; ++j) {
      for (int i = 0; i < n_ts; ++i) {
        int ts = gsl_rng_uniform_int(rng, n_ts);
        for (int k = 0; k < n_pt[j]; ++k) {
          int pt = gsl_rng_uniform_int(rng, n_pt[j]);
          double xNew = cnf[j][pt][ts] + gsl_ran_gaussian_ziggurat(rng, displaceSigma);
          double dS = getSliceActionDiff(j, pt, ts, xNew);
          if (gsl_rng_uniform(rng) < exp(-dS)) {
            cnf[j][pt][ts] = xNew;
            S += dS;
            ++nAccepts;
          }
          ++nAttempts;
        }
      }
    }
  }
  cout << "Warmup:\t" << nAccepts << "/" << nAttempts << " = " << 100.0*double(nAccepts)/double(nAttempts) << " percent acceptance" << endl;
  cout << "S = " << S << " ?= " << getTotalAction() << endl;

  vector<double> eLocals(lEnergy, 0.0);

  nAttempts = 0; nAccepts = 0;
  n = 0;
  time_t endTime = time(NULL) + runSeconds;
  while (time(NULL) < endTime) {
    for (int j = 0; j < n_sp; ++j) {
      for (int i = 0; i < n_ts; ++i) {
        int ts = gsl_rng_uniform_int(rng, n_ts);
        for (int k = 0; k < n_pt[j]; ++k) {
          int pt = gsl_rng_uniform_int(rng, n_pt[j]);
          double xNew = cnf[j][pt][ts] + gsl_ran_gaussian_ziggurat(rng, displaceSigma);
          double dS = getSliceActionDiff(j, pt, ts, xNew);
          if (gsl_rng_uniform(rng) < exp(-dS)) {
            cnf[j][pt][ts] = xNew;
            S += dS;
            ++nAccepts;
          }
          ++nAttempts;
        }
      }
    }
    eLocals[n % lEnergy] = getLocalEnergy();
    ++n;
  }
  cout << "Main run:\t" << nAccepts << "/" << nAttempts << " = " << 100.0*double(nAccepts)/double(nAttempts) << " percent acceptance" << endl;
  cout << "S = " << S << " ?= " << getTotalAction() << endl;

  //double mean = accumulate(eLocals.begin(), eLocals.end(), 0.0)/double(lEnergy);
  double mean = gsl_stats_mean(eLocals.data(), 1, lEnergy);
  double stdev = gsl_stats_sd_m(eLocals.data(), 1, lEnergy, mean);
  double kappa = gsl_stats_lag1_autocorrelation_m(eLocals.data(), 1, lEnergy, mean);
  // FIXME: cuts in between ... use circular_buffer instead
  double sterr = sqrt(kappa/double(lEnergy))*stdev;
  cout << "Energy = " << mean << " ± " << sterr << endl;

  for (int j = 0; j < n_sp; ++j) {
    for (int k = 0; k < n_pt[j]; ++k) delete cnf[j][k];
    delete cnf[j];
  }
  delete cnf;

  gsl_rng_free(rng);

  return 0;
}
