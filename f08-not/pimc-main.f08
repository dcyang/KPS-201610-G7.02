      module ingredients
        use ieee_arithmetic
        implicit none
        real, parameter :: T = 0.5
        integer, parameter :: n_sp = 2, n_pt(2) = (/ 13, 7 /)
        real, parameter :: mass = 1.0, omega = 1.0, g = 4.0

        integer, parameter :: n_ts = 64, nWarmup = 1000
        real, parameter :: tWall = 30.0
        integer, parameter :: lEnergy = 1000

        real :: beta, tau, lam, lam_red
        real :: displaceSigma2, displaceSigma
        real :: omega2

        real :: s, z
        real :: pi, sqrtPi

        real, allocatable :: cnf(:,:,:)

        public cnf

        contains

        real function get_total_action() result(retval)
          implicit none
          real :: kin, U1, U2a, U2b
          integer :: i, j, k, j1, k1, i_
          real :: dx, dy, r, dR

          kin = 0.0
          U1 = 0.0
          U2a = 0.0
          U2b = 0.0

          do j = 1, n_sp
            do i = 1, n_ts
              i_ = modulo(i, n_ts) + 1
              do k = 2, n_pt(j)
                dx = cnf(j,k,i) - cnf(j,k-1,i)
                dy = cnf(j,k,i_) - cnf(j,k-1,i_)
                if (dx <= 0.0 .or. dy <= 0.0) then
                  !U2a = huge(0.0)       ! fixme: not quite infinity
                  U2a = ieee_value(U2a, ieee_positive_inf)
                  exit
                else
                  U2a = U2a - log(1.0 - exp(-dx*dy/displaceSigma2))
                endif
              enddo
            enddo
          enddo

          do j = 1, n_sp
            do i = 1, n_ts
              i_ = modulo(i, n_ts) + 1
              do k = 1, n_pt(j)
                dx = cnf(j,k,i_) - cnf(j,k,i)
                kin = kin+0.5*log(pi*(displaceSigma2+displaceSigma2)) &
     &                + (dx*dx/(displaceSigma2+displaceSigma2))
                !print *, "kin:", j, i, k, kin
              enddo
            enddo
          enddo
          !print *, "term =", kin, j, i, k, dx

          do j = 1, n_sp
            do i = 1, n_ts
              i_ = modulo(i, n_ts) + 1
              do k = 1, n_pt(j)
                U1 = U1 + 0.25*tau*(cnf(j,k,i_)*cnf(j,k,i_) &
     &               + cnf(j,k,i)*cnf(j,k,i))
              enddo
            enddo
          enddo

          do j = 1, n_sp
            do j1 = j+1, n_sp
              do i = 1, n_ts
                i_ = modulo(i, n_ts) + 1
                do k = 1, n_pt(j)
                  do k1 = 1, n_pt(j1)
                    dx = cnf(j1,k1,i) - cnf(j,k,i)
                    dy = cnf(j1,k1,i_) - cnf(j,k,i_)
                    r = z*(abs(dx) + abs(dy))
                    dR = dx - dy
                    U2b = U2b - log(1.0 + sqrtPi*s*exp(s*(s-r-r) &
     &                    + z*z*dR*dR)*erfc(r-s))
                  enddo
                enddo
              enddo
            enddo
          enddo

          !print *, kin, U1, U2a, U2b, (kin + U1 + U2a + U2b)
          retval = kin + U1 + U2a + U2b
        end function

        real function get_slice_action_diff(sp, pt, ts0, xNew) &
     &  result(retval)
          implicit none
          integer, intent(in) :: sp, pt, ts0
          real, intent(in) :: xNew
          real :: dK, dU1, dU2a, dU2b
          real :: dx9L, dx1L, dxL, dx9R, dx1R, dxR
          real :: dxLNew, dxRNew, dxNew, dxOld
          real :: dx9, dx1, r, dR
          integer :: ts9, ts1, sp1, pt1
          logical :: foundL, foundR

          dK = 0.0
          dU1 = 0.0
          dU2a = 0.0
          dU2b = 0.0

          ts9 = modulo(ts0 - 2 + n_ts, n_ts) + 1
          ts1 = modulo(ts0, n_ts) + 1

          foundL = pt > 1
          foundR = pt < n_pt(sp)
          if (foundL .and. foundR) then
            dxLNew = xNew - cnf(sp,pt-1,ts0)
            dxRNew = cnf(sp,pt+1,ts0) - xNew
            if (dxLNew <= 0.0 .or. dxRNew <= 0.0) then
              dU2a = ieee_value(dU2a, ieee_positive_inf)
            else
              dx9L = cnf(sp,pt,ts9) - cnf(sp,pt-1,ts9)
              dx1L = cnf(sp,pt,ts1) - cnf(sp,pt-1,ts1)
              dxL = cnf(sp,pt,ts0) - cnf(sp,pt-1,ts0)
              dx9R = cnf(sp,pt+1,ts9) - cnf(sp,pt,ts9)
              dx1R = cnf(sp,pt+1,ts1) - cnf(sp,pt,ts1)
              dxR = cnf(sp,pt+1,ts0) - cnf(sp,pt,ts0)

              dU2a = dU2a + &
     &             (-log(1.0 - exp(-dx9L*dxLNew/displaceSigma2)) &
     &              -log(1.0 - exp(-dx9R*dxRNew/displaceSigma2)) &
     &              -log(1.0 - exp(-dxLNew*dx1L/displaceSigma2)) &
     &              -log(1.0 - exp(-dxRNew*dx1R/displaceSigma2)))
              dU2a = dU2a - &
     &             (-log(1.0 - exp(-dx9L*dxL/displaceSigma2)) &
     &              -log(1.0 - exp(-dx9R*dxR/displaceSigma2)) &
     &              -log(1.0 - exp(-dxL*dx1L/displaceSigma2)) &
     &              -log(1.0 - exp(-dxR*dx1R/displaceSigma2)))
            endif
          elseif (foundL) then
            dxLNew = xNew - cnf(sp,pt-1,ts0)
            if (dxLNew <= 0.0) then
              dU2a = ieee_value(dU2a, ieee_positive_inf)
            else
              dx9L = cnf(sp,pt,ts9) - cnf(sp,pt-1,ts9)
              dx1L = cnf(sp,pt,ts1) - cnf(sp,pt-1,ts1)
              dxL = cnf(sp,pt,ts0) - cnf(sp,pt-1,ts0)

              dU2a = dU2a + &
     &             (-log(1.0 - exp(-dx9L*dxLNew/displaceSigma2)) &
     &              -log(1.0 - exp(-dxLNew*dx1L/displaceSigma2)))
              dU2a = dU2a - &
     &             (-log(1.0 - exp(-dx9L*dxL/displaceSigma2)) &
     &              -log(1.0 - exp(-dxL*dx1L/displaceSigma2)))
            endif
          elseif (foundR) then
            dxRNew = cnf(sp,pt+1,ts0) - xNew
            if (dxRNew <= 0.0) then
              dU2a = ieee_value(dU2a, ieee_positive_inf)
            else
              dx9R = cnf(sp,pt+1,ts9) - cnf(sp,pt,ts9)
              dx1R = cnf(sp,pt+1,ts1) - cnf(sp,pt,ts1)
              dxR = cnf(sp,pt+1,ts0) - cnf(sp,pt,ts0)

              dU2a = dU2a + &
     &             (-log(1.0 - exp(-dx9R*dxRNew/displaceSigma2)) &
     &              -log(1.0 - exp(-dxRNew*dx1R/displaceSigma2)))
              dU2a = dU2a - &
     &             (-log(1.0 - exp(-dx9R*dxR/displaceSigma2)) &
     &              -log(1.0 - exp(-dxR*dx1R/displaceSigma2)))
            endif
          !else nothing
          endif

          dxNew = xNew - cnf(sp,pt,ts9)
          dxOld = cnf(sp,pt,ts0) - cnf(sp,pt,ts9)
          dK = dK + (dxNew*dxNew - dxOld*dxOld)/(2.0*displaceSigma2)
          dxNew = cnf(sp,pt,ts1) - xNew
          dxOld = cnf(sp,pt,ts1) - cnf(sp,pt,ts0)
          dK = dK + (dxNew*dxNew - dxOld*dxOld)/(2.0*displaceSigma2)

          dU1 = dU1 + &
     &          0.5 * tau * (xNew*xNew - cnf(sp,pt,ts0)*cnf(sp,pt,ts0))

          do sp1 = 1, sp-1
            do pt1 = 1, n_pt(sp1)
              dx9 = cnf(sp,pt,ts9) - cnf(sp1,pt1,ts9)
              dx1 = cnf(sp,pt,ts1) - cnf(sp1,pt1,ts1)
              dxOld = cnf(sp,pt,ts0) - cnf(sp1,pt1,ts0)
              dxNew = xNew - cnf(sp1,pt1,ts0)

              ! addition of a negative term
              r = z*(abs(dx9) + abs(dxNew))
              dR = dx9 - dxNew
              dU2b = du2b - log(1.0 + &
     &               sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
              r = z*(abs(dxNew) + abs(dx1))
              dR = dxNew - dx1
              dU2b = du2b - log(1.0 + &
     &               sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
              ! subtraction of a negative term
              r = z*(abs(dx9) + abs(dxOld))
              dR = dx9 - dxOld
              dU2b = du2b + log(1.0 + &
     &               sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
              r = z*(abs(dxOld) + abs(dx1))
              dR = dxOld - dx1
              dU2b = du2b + log(1.0 + &
     &               sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
            enddo
          enddo
          do sp1 = sp+1, n_sp
            do pt1 = 1, n_pt(sp1)
              dx9 = cnf(sp,pt,ts9) - cnf(sp1,pt1,ts9)
              dx1 = cnf(sp,pt,ts1) - cnf(sp1,pt1,ts1)
              dxOld = cnf(sp,pt,ts0) - cnf(sp1,pt1,ts0)
              dxNew = xNew - cnf(sp1,pt1,ts0)

              ! addition of a negative term
              r = z*(abs(dx9) + abs(dxNew))
              dR = dx9 - dxNew
              dU2b = du2b - log(1.0 + &
     &               sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
              r = z*(abs(dxNew) + abs(dx1))
              dR = dxNew - dx1
              dU2b = du2b - log(1.0 + &
     &               sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
              ! subtraction of a negative term
              r = z*(abs(dx9) + abs(dxOld))
              dR = dx9 - dxOld
              dU2b = du2b + log(1.0 + &
     &               sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
              r = z*(abs(dxOld) + abs(dx1))
              dR = dxOld - dx1
              dU2b = du2b + log(1.0 + &
     &               sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
            enddo
          enddo

          !print *, dK, dU1, dU2a, dU2b, (dK + dU1 + dU2a + dU2b)
          retval = dK + dU1 + dU2a + dU2b
        end function

        real function get_local_energy() result(retval)
          implicit none
          real :: kin, U1, U2a, U2b
          real :: tc, tc_buffer
          integer :: i, j, k, i_, j1, k1
          real :: dx, dy, r, dR, buf1, buf2

          kin = 0.0
          U1 = 0.0
          U2a = 0.0
          U2b = 0.0
          tc_buffer = 0.0

          ! pairwise like species
          do j = 1, n_sp
            do i = 1, n_ts
              i_ = modulo(i, n_ts) + 1
              do k = 2, n_pt(j)
                dx = cnf(j,k,i) - cnf(j,k-1,i)
                dy = cnf(j,k,i_) - cnf(j,k-1,i_)
                tc = dx*dy / (displaceSigma2 * tau * &
     &            (exp(dx*dy/displaceSigma2) - 1.0) * real(n_ts))
                U2a = U2a + dx*dy / (displaceSigma2 * tau * &
     &            (exp(dx*dy/displaceSigma2) - 1.0) * real(n_ts)) - tc
                tc_buffer = tc_buffer + tc
              enddo
            enddo
          enddo

          ! external (harmonic)
          do j = 1, n_sp
            do i = 1, n_ts
              i_ = modulo(i, n_ts) + 1
              do k = 1, n_pt(j)
                tc = -0.25 * (cnf(j,k,i_)*cnf(j,k,i_) + &
     &            cnf(j,k,i)*cnf(j,k,i)) / real(n_ts)
                U1 = U1 + 0.25 * (cnf(j,k,i_)*cnf(j,k,i_) + &
     &            cnf(j,k,i)*cnf(j,k,i)) / real(n_ts) - tc
                tc_buffer = tc_buffer + tc
              enddo
            enddo
          enddo

          ! pairwise unlike species
          do j = 1, n_sp
            do j1 = j+1, n_sp
              do i = 1, n_ts
                i_ = modulo(i, n_ts) + 1
                do k = 1, n_pt(j)
                  do k1 = 1, n_pt(j1)
                    dx = cnf(j1,k1,i) - cnf(j,k,i)
                    dy = cnf(j1,k1,i_) - cnf(j,k,i_)
                    r = z*(abs(dx) + abs(dy))
                    dR = dx - dy
                    buf1 = exp(s*(s-r-r)+z*z*dR*dR)
                    buf2 = erfc(r-s)
                    tc = lam_red*sqrtPi*s*buf1* &
     &                ((1.0+2.0*(s*s-2.0*s*r+z*z*dR*dR))*buf2 &
     &                 +(2.0/sqrtPi)*exp(-(r-s)*(r-s))*(s-r)) &
     &                /(2.0*tau*lam_red*(1.0+sqrtPi*s*buf1*buf2)*n_ts)
                    U2b = U2b + (-sqrtPi*s*buf1* &
     &                ((1.0+2.0*(s*s-z*z*dR*dR))*buf2 &
     &                 +(2.0/sqrtPi)*exp(-(r-s)*(r-s))*(r+s)) &
     &                /(2.0*tau*(1.0+sqrtPi*s*buf1*buf2)*n_ts) - tc)
                    tc_buffer = tc_buffer + tc
                  enddo
                enddo
              enddo
            enddo
          enddo

          ! kinetic
          do j = 1, n_sp
            do i = 1, n_ts
              i_ = modulo(i, n_ts) + 1
              do k = 1, n_pt(j)
                dx = cnf(j,k,i_) - cnf(j,k,i)
                kin = kin + 0.5 * T * (1.0 - real(n_ts) * T * dx * dx)
              enddo
            enddo
          enddo
          kin = kin + tc_buffer

          retval = kin + U1 + U2a + U2b
        end function
      end module


      program main
        use ingredients
        implicit none
        real :: total_action, dS, u
        real :: xNew
        real :: box_hwidth = 3.0
        integer :: i, j, k, n
        integer :: ts, pt
        integer :: nAccepts, nAttempts
        integer(kind=8) :: clock_A, clock_B, clock_rate
        real, allocatable :: eLocals(:)

        pi = 4.*atan(1.)
        sqrtPi = sqrt(pi)

        beta = 1.0 / T
        tau = beta / real(n_ts)
        lam = 0.5 / mass
        lam_red = lam + lam

        displaceSigma2 = 2.0 * lam * tau
        displaceSigma = sqrt(displaceSigma2)
        s = 0.5 * g * sqrt(tau/lam_red)
        z = 1.0 / sqrt(4.0 * lam_red * tau)

        allocate(cnf(n_sp,maxval(n_pt),n_ts))

        do j = 1, n_sp
          if (n_pt(j) .eq. 1) then
            cnf(j,k,:) = 0.0
          else
            do k = 1, n_pt(j)
              cnf(j,k,:)=box_hwidth*(2.0*real(k-1)/real(n_pt(j)-1)-1.0)
              !print *, j, k, cnf(j,k,:)
            enddo
          endif
        enddo

        total_action = get_total_action()
        print *, "initial action =", total_action
        nAccepts = 0
        nAttempts = 0
        do n = 1, nWarmup
          do j = 1, n_sp
            do i = 1, n_ts
              call random_number(u)
              ts = 1 + floor(n_ts * u)
              do k = 1, n_pt(j)
                call random_number(u)
                pt = 1 + floor(n_pt(j) * u)

                call random_number(u)
                ! really should make this gaussian
                xNew = cnf(j,pt,ts) + 2.0*displaceSigma*(u-0.5) 
                dS = get_slice_action_diff(j, pt, ts, xNew)

                call random_number(u)
                if (u < exp(-dS)) then
                  cnf(j,pt,ts) = xNew
                  total_action = total_action + dS
                  nAccepts = nAccepts + 1
                endif
                nAttempts = nAttempts + 1
              enddo
            enddo
          enddo
        enddo

        print *, nAccepts, "/", nAttempts, "=", &
     &    100.0*real(nAccepts)/real(nAttempts), " percent acceptance"

        allocate(eLocals(lEnergy))
        nAccepts = 0
        nAttempts = 0
        n = 0
        call system_clock(clock_A, clock_rate)
        clock_B = clock_A + tWall*clock_rate
        do while (clock_A .le. clock_B)
          do j = 1, n_sp
            do i = 1, n_ts
              call random_number(u)
              ts = 1 + floor(n_ts * u)
              do k = 1, n_pt(j)
                call random_number(u)
                pt = 1 + floor(n_pt(j) * u)

                call random_number(u)
                ! really should make this gaussian
                xNew = cnf(j,pt,ts) + 2.0*displaceSigma*(u-0.5) 
                dS = get_slice_action_diff(j, pt, ts, xNew)

                call random_number(u)
                if (u < exp(-dS)) then
                  cnf(j,pt,ts) = xNew
                  total_action = total_action + dS
                  nAccepts = nAccepts + 1
                endif
                nAttempts = nAttempts + 1
              enddo
            enddo
          enddo
          eLocals(modulo(n, lEnergy)+1) = get_local_energy()
          n = n + 1

          call system_clock(clock_A)
        enddo

        print *, nAccepts, "/", nAttempts, "=", &
     &    100.0*real(nAccepts)/real(nAttempts), " percent acceptance"
        print *, "final action =", total_action, &
     &    "?=", get_total_action()
        print *, "average Energy =", sum(eLocals)/real(lEnergy)

        deallocate(eLocals)
        deallocate(cnf)
      end program
