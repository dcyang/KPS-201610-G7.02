#!/usr/bin/env julia

import StatsBase

T = 0.5
n_sp = 2
n_pt = [13,7]

mass = 1.0
ω = 1.0
g = 4.0

n_ts = 64
nWarmup = 1000
tWall = 300.0

lEnergy = 1000

β = 1.0 / T
τ = β / n_ts
λ = 0.5 / mass
λ_reduced = λ * 2.0
displaceSigma2 = 2.0 * λ * τ
displaceSigma = sqrt(displaceSigma2)
sqrtPi = sqrt(π)
ω2 = ω * ω

s = 0.5*g*sqrt(τ/λ_reduced)
z = 1.0/sqrt(4.0*λ_reduced*τ)

cnf = []
for j in 1:n_sp
    push!(cnf, sort(randn(n_pt[j],n_ts),1) * 2.0)
end
#println(cnf)

function getTotalAction()
    K = U1 = U2a = U2b = 0.0

    # pairwise like species (fermi nodal)
    for j in 1:n_sp
        for i in 1:n_ts
            i_ = i + 1
            if i_ > n_ts
                i_ -= n_ts
            end
            for k in 2:n_pt[j]
                dx = cnf[j][k,i] - cnf[j][k-1,i]
                dy = cnf[j][k,i_] - cnf[j][k-1,i_]
                if dx <= 0.0 || dy <= 0.0
                    return Inf
                end
                U2a -= log1p(-exp(-dx*dy/displaceSigma2))
            end
        end
    end

    # ||
    for j in 1:n_sp
        for i in 1:n_ts
            i_ = i + 1
            if i_ > n_ts
                i_ -= n_ts
            end
            for k in 1:n_pt[j]
                dx = cnf[j][k,i_] - cnf[j][k,i]
                K += 0.5 * log(pi*(displaceSigma2+displaceSigma2)) + (dx*dx/(displaceSigma2+displaceSigma2))
            end
        end
    end
    
    # external (harmonic)
    for j in 1:n_sp
        for i in 1:n_ts
            i_ = i + 1
            if i_ > n_ts
                i_ -= n_ts
            end
            for k in 1:n_pt[j]
                U1 += 0.25 * τ * (cnf[j][k,i_]*cnf[j][k,i_] + cnf[j][k,i]*cnf[j][k,i])
            end
        end
    end
    
    # pairwise unlike species
    for j in 1:n_sp
        for j1 in j+1:n_sp
            for i in 1:n_ts
                i_ = i + 1
                if i_ > n_ts
                    i_ -= n_ts
                end
                for k in 1:n_pt[j]
                    for k1 in 1:n_pt[j1]
                        dx = cnf[j1][k1,i] - cnf[j][k,i]
                        dy = cnf[j1][k1,i_] - cnf[j][k,i_]
                        r = z*(abs(dx) + abs(dy))
                        dR = dx - dy
                        U2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
                    end
                end
            end
        end
    end
    
    println(K, " ", U1, " ", U2a, " ", U2b, "\t", K+U1+U2a+U2b)
    return K+U1+U2a+U2b
end

function getSliceActionDiff(sp, pt, ts0, xNew)
    dK = dU1 = dU2a = dU2b = 0.0
    ts9 = ts0 - 1
    if ts9 < 1
        ts9 += n_ts
    end
    ts1 = ts0 + 1
    if ts1 > n_ts
        ts1 -= n_ts
    end

    foundL = pt > 1
    foundR = pt < n_pt[sp]
    if foundL && foundR
        dxLNew = xNew - cnf[sp][pt-1,ts0]
        dxRNew = cnf[sp][pt+1,ts0] - xNew
        if dxLNew <= 0.0 || dxRNew <= 0.0
            return Inf
        end

        dx9L = cnf[sp][pt,ts9] - cnf[sp][pt-1,ts9]
        dx1L = cnf[sp][pt,ts1] - cnf[sp][pt-1,ts1]
        dxL = cnf[sp][pt,ts0] - cnf[sp][pt-1,ts0]
        dx9R = cnf[sp][pt+1,ts9] - cnf[sp][pt,ts9]
        dx1R = cnf[sp][pt+1,ts1] - cnf[sp][pt,ts1]
        dxR = cnf[sp][pt+1,ts0] - cnf[sp][pt,ts0]
        
        dU2a += (-log1p(-exp(-dx9L*dxLNew/displaceSigma2))
                 -log1p(-exp(-dx9R*dxRNew/displaceSigma2))
                 -log1p(-exp(-dxLNew*dx1L/displaceSigma2))
                 -log1p(-exp(-dxRNew*dx1R/displaceSigma2)))
        dU2a -= (-log1p(-exp(-dx9L*dxL/displaceSigma2))
                 -log1p(-exp(-dx9R*dxR/displaceSigma2))
                 -log1p(-exp(-dxL*dx1L/displaceSigma2))
                 -log1p(-exp(-dxR*dx1R/displaceSigma2)))
    elseif foundL
        dxLNew = xNew - cnf[sp][pt-1,ts0]
        if dxLNew <= 0.0
            return Inf
        end

        dx9L = cnf[sp][pt,ts9] - cnf[sp][pt-1,ts9]
        dx1L = cnf[sp][pt,ts1] - cnf[sp][pt-1,ts1]
        dxL = cnf[sp][pt,ts0] - cnf[sp][pt-1,ts0]

        dU2a += (-log1p(-exp(-dx9L*dxLNew/displaceSigma2))
                 -log1p(-exp(-dxLNew*dx1L/displaceSigma2)))
        dU2a -= (-log1p(-exp(-dx9L*dxL/displaceSigma2))
                 -log1p(-exp(-dxL*dx1L/displaceSigma2)))
    elseif foundR
        dxRNew = cnf[sp][pt+1,ts0] - xNew
        if dxRNew <= 0.0
            return Inf
        end

        dx9R = cnf[sp][pt+1,ts9] - cnf[sp][pt,ts9]
        dx1R = cnf[sp][pt+1,ts1] - cnf[sp][pt,ts1]
        dxR = cnf[sp][pt+1,ts0] - cnf[sp][pt,ts0]
        
        dU2a += (-log1p(-exp(-dx9R*dxRNew/displaceSigma2))
                 -log1p(-exp(-dxRNew*dx1R/displaceSigma2)))
        dU2a -= (-log1p(-exp(-dx9R*dxR/displaceSigma2))
                 -log1p(-exp(-dxR*dx1R/displaceSigma2)))
    #else nothing
    end
    
    dxNew = xNew - cnf[sp][pt,ts9]
    dxOld = cnf[sp][pt,ts0] - cnf[sp][pt,ts9]
    dK += (dxNew*dxNew - dxOld*dxOld)/(displaceSigma2+displaceSigma2)
    dxNew = cnf[sp][pt,ts1] - xNew
    dxOld = cnf[sp][pt,ts1] - cnf[sp][pt,ts0]
    dK += (dxNew*dxNew - dxOld*dxOld)/(displaceSigma2+displaceSigma2)
    
    dU1 += 0.5*τ*(xNew*xNew - cnf[sp][pt,ts0]*cnf[sp][pt,ts0])
    
    for sp1 in [1:sp-1;sp+1:n_sp]
        for pt1 in 1:n_pt[sp1]
            dx9 = cnf[sp][pt,ts9] - cnf[sp1][pt1,ts9]
            dx1 = cnf[sp][pt,ts1] - cnf[sp1][pt1,ts1]
            dx0 = cnf[sp][pt,ts0] - cnf[sp1][pt1,ts0]
            dxNew = xNew - cnf[sp1][pt1,ts0]
            
            # addition of a negative term
            r = z*(abs(dx9) + abs(dxNew))
            dR = dx9 - dxNew
            dU2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
            r = z*(abs(dxNew) + abs(dx1))
            dR = dxNew - dx1
            dU2b -= log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))

            # subtraction of a negative term
            r = z*(abs(dx9) + abs(dx0))
            dR = dx9 - dx0
            dU2b += log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
            r = z*(abs(dx0) + abs(dx1))
            dR = dx0 - dx1
            dU2b += log1p(sqrtPi*s*exp(s*(s-r-r)+z*z*dR*dR)*erfc(r-s))
        end
    end

    return dK+dU1+dU2a+dU2b
end

function getLocalEnergy()
    K = U1 = U2a = U2b = 0.0
    tc_buffer = 0.0
    
    # pairwise like species
    for j in 1:n_sp
        for i in 1:n_ts
            i_ = i + 1
            if i_ > n_ts
                i_ -= n_ts
            end
            for k in 2:n_pt[j]
                dx = cnf[j][k,i] - cnf[j][k-1,i]
                dy = cnf[j][k,i_] - cnf[j][k-1,i_]
                tc = dx*dy / (displaceSigma2 * τ * (exp(dx*dy/displaceSigma2) - 1.0) * n_ts)
                U2a += dx*dy / (displaceSigma2 * τ * (exp(dx*dy/displaceSigma2) - 1.0) * n_ts) - tc
                tc_buffer += tc
            end
        end
    end

    # external (harmonic)
    for j in 1:n_sp
        for i in 1:n_ts
            i_ = i + 1
            if i_ > n_ts
                i_ -= n_ts
            end
            for k in 1:n_pt[j]
                tc = -0.25 * (cnf[j][k,i_]*cnf[j][k,i_] + cnf[j][k,i]*cnf[j][k,i]) / n_ts
                U1 += 0.25 * (cnf[j][k,i_]*cnf[j][k,i_] + cnf[j][k,i]*cnf[j][k,i]) / n_ts - tc
                tc_buffer += tc
            end
        end
    end

    # pairwise unlike species
    for j in 1:n_sp
        for j1 in j+1:n_sp
            for i in 1:n_ts
                i_ = i + 1
                if i_ > n_ts
                    i_ -= n_ts
                end
                for k in 1:n_pt[j]
                    for k1 in 1:n_pt[j1]
                        dx = cnf[j1][k1,i] - cnf[j][k,i]
                        dy = cnf[j1][k1,i_] - cnf[j][k,i_]
                        r = z*(abs(dx) + abs(dy))
                        dR = dx - dy
                        buf1 = exp(s*(s-r-r)+z*z*dR*dR)
                        buf2 = erfc(r-s)
                        tc = λ_reduced*sqrtPi*s*buf1*((1.0+2.0*(s*s-2*s*r+z*z*dR*dR))*buf2+(2.0/sqrtPi)*exp(-(r-s)*(r-s))*(s-r))/(2.0*τ*λ_reduced*(1.0+sqrtPi*s*buf1*buf2)*n_ts)
                        U2b += (-sqrtPi*s*buf1*((1.0+2.0*(s*s-z*z*dR*dR))*buf2 + (2.0/sqrtPi)*exp(-(r-s)*(r-s))*(r+s))/(2.0*τ*(1.0+sqrtPi*s*buf1*buf2)*n_ts) - tc)
                        tc_buffer += tc
                    end
                end
            end
        end
    end

    # kinetic
    for j in 1:n_sp
        for i in 1:n_ts
            i_ = i + 1
            if i_ > n_ts
                i_ -= n_ts
            end
            for k in 1:n_pt[j]
                dx = cnf[j][k,i_] - cnf[j][k,i]
                K += 0.5 * T * (1.0 - n_ts * T * dx * dx)
            end
        end
    end
    K += tc_buffer
    
    #println(K, " ", U1, " ", U2a, " ", U2b, "\t", K+U1+U2a+U2b)
    return K+U1+U2a+U2b
end

S = getTotalAction()
@assert isfinite(S)

nAccepts = nAttempts = 0
for n in 1:nWarmup
    for j in 1:n_sp
        for i in 1:n_ts
            ts = rand(1:n_ts)
            for k in 1:n_pt[j]
                pt = rand(1:n_pt[j])
                xNew = cnf[j][pt,ts] + displaceSigma*randn()
                dS = getSliceActionDiff(j, pt, ts, xNew)
                
                if rand() < exp(-dS)
                    cnf[j][pt,ts] = xNew
                    S += dS
                    nAccepts += 1
                end
                nAttempts += 1
            end
        end
    end
end
println(nAccepts,"/",nAttempts, " = ", 100*nAccepts/nAttempts, " percent acceptance")
println(S)

S = getTotalAction()
@assert isfinite(S)

nAccepts = nAttempts = 0
eLocals = zeros(lEnergy);
tEnd = now() + Dates.Second(tWall)
n = 0
while now() < tEnd
    for j in 1:n_sp
        for i in 1:n_ts
            ts = rand(1:n_ts)
            for k in 1:n_pt[j]
                pt = rand(1:n_pt[j])
                xNew = cnf[j][pt,ts] + displaceSigma*randn()
                dS = getSliceActionDiff(j, pt, ts, xNew)
                
                if rand() < exp(-dS)
                    cnf[j][pt,ts] = xNew
                    S += dS
                    nAccepts += 1
                end
                nAttempts += 1
            end
        end
    end
    eLocals[(n % lEnergy) + 1] = getLocalEnergy()
    n += 1
end
println(nAccepts,"/",nAttempts, " = ", 100*nAccepts/nAttempts, " percent acceptance")
println(S)

#output statistics
eMean, eStdev = StatsBase.mean_and_std(eLocals)
eAC = StatsBase.autocor(eLocals)
κ = 1 + 2.0*sum(eAC[2:end])
println(eMean, " ± ", eStdev*sqrt(κ/lEnergy))
